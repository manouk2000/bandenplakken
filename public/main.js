window.onload = function(){
  const placeholders = document.getElementsByClassName("js--placeholder");
  const scene = document.getElementById("js--scene");
  const cursor = document.getElementById("js--cursor");
  const uitleg = document.getElementById("js--uitleg");
  const binnenband = document.getElementById("js--binnenband");
  const buitenband = document.getElementById("js--buitenband");
  const fietspomp = document.getElementById("js--fietspomp");
  const hendel = document.getElementById("js--hendel");
  const ventiel = document.getElementById("js--ventiel");
  const spreken = document.getElementById("spreken");
  const uitlegPlane = document.getElementById("js--plane-tekst");
  const verderKnop = document.getElementById("js--plane-button");
  const garageDeur = document.getElementById('js--deur');
  const garageDeurEinde = document.getElementById('js--eindedeur');
  const camera = document.getElementById("js--camera");
  const uitlegBegin = document.getElementById("js--uitlegBegin");
  const uitlegStarten = document.getElementById('js--uitlegStarten');
  const bronnen = document.getElementById('js--bronnen');

  var leeg = false;
  var buitenBandEraf = false;
  var binnenBandEraf = false;
  var binnenBandGeschuurd = false;
  var bandGeplakt = false;
  var binnenBandErop = false;
  var gaatjeGevonden = false;

  let hold = null;

  verderKnop.onmouseenter = function() {
    uitlegPlane.setAttribute("visible", "false");
    uitlegPlane.setAttribute("animation__position", "property: position; from: 0 2.5 10.5; to: 0 30 10.5; dur: 4000;");
    verderKnop.setAttribute("visible", "false");
    verderKnop.setAttribute("animation__position", "property: position; from: 0 0.50 10.51; to: 0 30 10.51; dur: 4000;");
    uitlegBegin.setAttribute("visible", "false");
    uitlegStarten.setAttribute("visible", "false");
    uitlegBegin.setAttribute("animation__position", "property: position; from: -2.45 3 12.50; to: -2.45 30 12.5; dur: 4000;");
    garageDeur.setAttribute("animation__position", "property: position; from: 0 0 10.1; to: 0 30 10.1; dur: 4000;");
    camera.setAttribute("animation__position", "property: position; from: 0 1.5 18; to: 0 1.5 0; dur: 5000; delay: 4000");
    setTimeout(function() {
      responsiveVoice.speak("Hallo en welkom bij banden plakken. Maak eerst de band leeg door naar het ventiel te kijken.", "Dutch Male");
    },9000);

  };

  //============================================================================
  // Oppakken van een object
  //============================================================================
  function addListeners() {
    var pickups = document.getElementsByClassName("js--pickup");
    const haak = document.getElementById("js--haak");
    const schuurpapier = document.getElementById("js--schuurpapier");
    const plakkertjes = document.getElementById("js--plakkertjes");
    for (let i = 0; i < pickups.length; i++) {
      pickups[i].addEventListener("click", function(evt){
        if (hold == null){
          if (this == haak) {
            hold = "haak";
            cursor.innerHTML += '<a-gltf-model id="js--hold" class="js--pickup" src="#haak" position="0.5 -0.75 -0.5" rotation="0 45 0" scale="0.5 0.5 0.5"></a-gltf-model>';
            if(binnenBandEraf == false){
              uitleg.setAttribute("value", "Haal aan de linkerkant de banden van de fiets eraf");
              responsiveVoice.speak("Haal aan de linkerkant de banden van de fiets eraf","Dutch Male");
            } else {
              uitleg.setAttribute("value", "Ik denk niet dat we deze nu nodig hebben, denk je ook niet?");
              responsiveVoice.speak("Ik denk niet dat we deze nu nodig hebben, denk je ook niet?", "Dutch Male");
            }
          } else if (this == schuurpapier) {
            hold = "schuurpapier";
            cursor.innerHTML += '<a-gltf-model id="js--hold" class="js--pickup" src="#schuurpapier" position="1 -1 -0.5" scale="0.5 0.5 0.5"></a-gltf-model>';
            if (binnenBandEraf == true) {
              uitleg.setAttribute("value", "Mooi zo, het schuurpapier hebben we nodig om de binnenband een beetje te schuren, zodat we de band beter kunnen plakken. Schuur dus nu de binnenband.");
              responsiveVoice.speak("Mooi zo, het schuurpapier hebben we nodig om de binnenband een beetje te schuren, zodat we de band beter kunnen plakken. Schuur dus nu de binnenband.", "Dutch Male");
            } else {
              uitleg.setAttribute("value", "Ik denk niet dat we deze nu nodig hebben, denk je ook niet?");
              responsiveVoice.speak("Ik denk niet dat we deze nu nodig hebben, denk je ook niet?", "Dutch Male");
            }
          } else if (this == plakkertjes) {
            hold = "plakkertjes";
            cursor.innerHTML += '<a-gltf-model id="js--hold" class="js--pickup" src="#plakkertje" position="1 -1 -0.5" rotation="0 90 0" scale="0.005 0.005 0.005"></a-gltf-model>';
            if (binnenBandGeschuurd == true) {
              uitleg.setAttribute("value", "Deze hebben we nodig, zorg ervoor dat je wat lijm doet op het plakkertje en dan het plakkertje op de band plakt.");
              responsiveVoice.speak("Deze hebben we nodig, zorg ervoor dat je wat lijm doet op het plakkertje en dan het plakkertje op de band plakt.", "Dutch Male");
            } else {
              uitleg.setAttribute("value", "Wil je nu gaan plakken? Lijkt me een beetje raar.");
              responsiveVoice.speak("Wil je nu gaan plakken? Lijkt me een beetje raar.", "Dutch Male");
            }
          }
          this.remove();
        }
      });
    }
  }

  function binnenBandErafHalen() {
    binnenband.addEventListener("click", function(evt){
      if (buitenBandEraf == true) {
        binnenband.setAttribute("animation__position", "property: position; from: -1 1 -4; to: 1 0 -2.5; dur: 2000;");
        binnenband.setAttribute("animation__rotation", "property: rotation; from: 0 0 0; to: -90 0 0 dur: 2000;");
        binnenBandEraf = true;
        uitleg.setAttribute("value", "De binnenband heb je er goed afgehaald. Tijd om te kijken waar het gaatje zit. Leg de haak weer terug op tafel.");
        responsiveVoice.speak("De binnenband heb je er goed afgehaald. Tijd om te kijken waar het gaatje zit. Leg de haak weer terug op tafel.", "Dutch Male");
        setTimeout(function () {
          uitleg.setAttribute("value", "We moeten de band in de bak met water doen met een beetje lucht, dan kunnen we namelijk goed zien waar het gaatje zit. Je ziet dan bubbeltjes.");
          responsiveVoice.speak("We moeten de band in de bak met water doen met een beetje lucht, dan kunnen we namelijk goed zien waar het gaatje zit. Je ziet dan bubbeltjes.", "Dutch Male");
        }, 10000);
        buitenBandEraf = null;
        bandInteractie();
       }
    });
  }

  addListeners();

  function buitenBandErafHalen(){
    buitenband.addEventListener("click", function(evt){
      if (leeg == true && hold == "haak") {
        ventiel.setAttribute("visible", "false");
        buitenband.setAttribute("animation", null);
        document.getElementById("js--hold").setAttribute("animation", "property: position; from: 0.5 -0.75 -0.5; to -15 -5 -8.5; dur: 2000");
        buitenband.setAttribute("animation__position", "property: position; from: -1 1 -4; to: -1 0 -2.5; dur: 2000;");
        buitenband.setAttribute("animation__rotation", "property: rotation; from: 0 0 0; to: -90 0 0 dur: 2000;");
        buitenBandEraf = true;
        uitleg.setAttribute("value", "De buitenband is er af. Haal nu de binnenband eraf");
        responsiveVoice.speak("De buitenband is er af. Haal nu de binnenband eraf", "Dutch Male");
        binnenBandErafHalen();
      }
     });
    ventiel.addEventListener("click", function(evt){
      if (leeg == false){
        document.getElementById("js--buitenband").setAttribute("animation__click", "property: scale; dur: 150; from: 1 1 1; to: 0.9 0.9 0.9");
        document.getElementById("legeband").play();
        setTimeout(function () {
          document.getElementById("legeband").pause();
        }, 1000);
        uitleg.setAttribute("value", "De buitenband is leeg. Pak de haak om de buitenband eraf te halen.");
        responsiveVoice.speak("De buitenband is leeg. Pak de haak om de buitenband eraf te halen.", "Dutch Male");
        leeg = true;
      }
     });
 }

 buitenBandErafHalen();

 function bandOppompen() {
   ventiel.addEventListener("click", function(evt){
     if (hold == "fietspomp") {
       buitenband.setAttribute("animation", "property: scale; dur: 2000; from: 0.9 0.9 0.9; to: 1 1 1;");
       hendel.setAttribute("animation__omhoog", "property: position; from: 0 2 0; to: 0 3 0; dur: 2000;");
       document.getElementById("pomp").play();
       hendel.setAttribute("animation__omlaag", "property: position; from: 0 3 0; to: 0 2 0; dur: 2000; delay: 2000;");
       hendel.setAttribute("animation__omhoog2", "property: position; from: 0 2 0; to: 0 3 0; dur: 2000; delay: 4000");
       hendel.setAttribute("animation__omlaag2", "property: position; from: 0 3 0; to: 0 2 0; dur: 2000; delay: 6000;");
       setTimeout(function () {
         uitleg.setAttribute("value", "Mooi, de band is opgepompt. Dat was het dan. Alle stappen om een band te plakken.");
         responsiveVoice.speak("Mooi, de band is opgepompt. Dat was het dan. Alle stappen om een band te plakken.", "Dutch Male");
       }, 8000);
     }
   });
   fietspomp.addEventListener("click", function(evt){
     hold = "fietspomp";
     fietspomp.setAttribute("animation__position", "property: position; from: 5 0 2.5; to: 0 0 -3; dur: 2000;");
     fietspomp.setAttribute("animation__rotation", "property: rotation; from: 0 0 0; to: 0 -90 0; dur: 2000;");
     setTimeout(function () {
       uitleg.setAttribute("value", "De pomp is gepakt, nu alleen nog de band oppompen. Kijk naar het ventiel om de band op te pompen.");
       responsiveVoice.speak("De pomp is gepakt, nu alleen nog de band oppompen. Kijk naar het ventiel om de band op te pompen.", "Dutch Male");
     }, 2000);

     garageDeurEinde.setAttribute("animation__position", "property: position; from: 0 0 10; to: 0 30 10; dur: 6000; delay: 10000;");
     camera.setAttribute("animation__position", "property: position; from: 0 1.5 0; to: 0 1.5 18; dur: 5000; delay: 15000");
     garageDeur.setAttribute("animation__position", "property: position; from: 0 30 10.1; to: 0 0 10.1; dur: 4000; delay: 20000");
     uitlegPlane.setAttribute("visible", "true");
     uitlegPlane.setAttribute("animation__position", "property: position; from: 0 30 10.5; to: 0 2.5 10.5; dur: 4000; delay: 21000");
     setTimeout(function () {
       bronnen.setAttribute("visible", "true");
     }, 25000);

   });
 }

 function buitenBandErop() {
   buitenband.addEventListener("click", function(evt){
    if (hold == null && binnenBandErop == true) {
      buitenband.setAttribute("animation__position", "property: position; from: -1 0 -2.5; to: -1 1 -4; dur: 2000;");
      buitenband.setAttribute("animation__rotation", "property: rotation; from: -90 0 0; to: 0 0 0; dur: 2000;");
      setTimeout(function () {
        uitleg.setAttribute("value", "Beide banden zitten er nu op, pak nu de fietspomp.");
        responsiveVoice.speak("Beide banden zitten er nu op, pak nu de fietspomp.", "Dutch Male");
      }, 2000);
      ventiel.setAttribute("visible", "true");
      binnenBandErop = null;
      bandOppompen();
      }
    });
 }

 function bandInteractie(){
  binnenband.addEventListener("click", function(evt){
    if (binnenBandGeschuurd == true && hold == "plakkertjes") {
      bandGeplakt = true;
      uitleg.setAttribute("value", "Nu de band is geplakt kunnen de banden weer op de fiets gezet worden.");
      responsiveVoice.speak("Nu de band is geplakt kunnen de banden weer op de fiets gezet worden.", "Dutch Male");
    }
    else if (hold == null && bandGeplakt == true) {
      binnenband.setAttribute("animation__position", "property: position; from: 1 0 -2.5; to: -1 1 -4; dur: 2000;");
      binnenband.setAttribute("animation__rotation", "property: rotation; from: -90 0 0; to: 0 0 0; dur: 2000;");
      binnenBandErop = true;
      bandGeplakt = null;
      setTimeout(function () {
        uitleg.setAttribute("value", "Nu kun je de buitenband ook weer op de fiets zetten.");
        responsiveVoice.speak("Nu kun je de buitenband ook weer op de fiets zetten.", "Dutch Male");
      }, 2000);
      buitenBandErop();
    }
    else if (hold == "schuurpapier" && gaatjeGevonden == true) {
      binnenBandGeschuurd = true;
      document.getElementById("schuurPapier").play();
      setTimeout(function () {
        document.getElementById("schuurPapier").pause();
      }, 2000);
      uitleg.setAttribute("value", "Nu kunnen we de band plakken, dus leg het schuurpapier weg en pak de plakkertjes van de tafel.");
      responsiveVoice.speak("Nu kunnen we de band plakken, dus leg het schuurpapier weg en pak de plakkertjes van de tafel.", "Dutch Male");
    }
    else {
      gaatjeGevonden = true;
      binnenband.setAttribute("animation__omhoog", "property: position; from: 1 0 -2.5; to: 1 1 -2.5; dur: 2000;");
      binnenband.setAttribute("animation__rechts", "property: position; from: 1 1 -2.5; to: 3 1 -2.5; dur: 2000; delay: 2000;");
      binnenband.setAttribute("animation__rotation", "property: rotation; from: -90 0 0; to: 35 -90 0; dur: 2000; delay: 4000;");
      binnenband.setAttribute("animation__omlaag", "property: position; from: 3 1 -2.5; to: 3 0.5 -2.5; dur: 2000; delay: 6000;");
      setTimeout(function () {
        var bubbels = document.getElementById("bubbels");
        bubbels.play();
        setTimeout(function () {
          bubbels.pause();
        }, 2000);
      }, 8000);
      binnenband.setAttribute("animation__rotation2", "property: rotation; from: 35 -90 0; to: 35 -90 -360; dur: 2000; delay: 8000;");
      binnenband.setAttribute("animation__omhoog2", "property: position; from: 3 0.5 -2.5; to: 3 1 -2.5; dur: 2000; delay: 10000;");
      binnenband.setAttribute("animation__links", "property: position; from: 3 1 -2.5; to: 1 1 -2.5; dur: 2000; delay: 12000;");
      binnenband.setAttribute("animation__rotation3", "property: rotation; from: 35 -90 -360; to: -90 0 0; dur: 2000; delay: 14000;");
      binnenband.setAttribute("animation__omlaag2", "property: position; from: 1 1 -2.5; to: 1 0 -2.5; dur: 2000; delay: 16000;");
      setTimeout(function () {
        binnenband.setAttribute("animation__omhoog", null);
        binnenband.setAttribute("animation__rechts", null);
        binnenband.setAttribute("animation__rotation", null);
        binnenband.setAttribute("animation__omlaag", null);
        binnenband.setAttribute("animation__rotation2", null);
        binnenband.setAttribute("animation__omhoog2", null);
        binnenband.setAttribute("animation__links", null);
        binnenband.setAttribute("animation__rotation3", null);
        binnenband.setAttribute("animation__omlaag2", null);
        uitleg.setAttribute("value", "Het gaatje is gevonden, dus we kunnen de band schuren waar het gaatje zit");
        responsiveVoice.speak("Het gaatje is gevonden, dus we kunnen de band schuren waar het gaatje zit", "Dutch Male");
      }, 18000);
    }
  });
  }
  //============================================================================
  // Object wat je vast hebt neerleggen
  //============================================================================
  for (let i = 0; i < placeholders.length; i++) {
    placeholders[i].addEventListener("click", function(evt){
     if (hold == "schuurpapier") { //Voor het schuurpapier
        let box = document.createElement("a-gltf-model");
        box.setAttribute("id", "js--schuurpapier");
        box.setAttribute("class", "js--pickup clickable");
        box.setAttribute("src", "#schuurpapier");
        box.setAttribute("scale", "0.5 0.5 0.5");
        box.setAttribute("position", "6.5 1.1 1.5");
        box.setAttribute("rotation", "0 0 25");
        scene.appendChild(box);
        addListeners();
        hold = null;
        document.getElementById('js--hold').remove();
      }
      else if (hold == "haak") { //Voor de haak
        let haakNeergelegd = document.createElement("a-gltf-model");
        haakNeergelegd.setAttribute("id", "js--haak");
        haakNeergelegd.setAttribute("class", "js--pickup clickable");
        haakNeergelegd.setAttribute("src", "#haak");
        haakNeergelegd.setAttribute("position", "7.6 1.2 0.8");
        haakNeergelegd.setAttribute("rotation", "-90 -20 10");
        scene.appendChild(haakNeergelegd);
        addListeners();
        hold = null;
        document.getElementById('js--hold').remove();

      } else if (hold == "plakkertjes") {
        let plakkertjesNeergelegd = document.createElement("a-gltf-model");
        plakkertjesNeergelegd.setAttribute("class", "js--pickup clickable");
        plakkertjesNeergelegd.setAttribute("id", "js--plakkertjes");
        plakkertjesNeergelegd.setAttribute("src", "#plakkertje");
        plakkertjesNeergelegd.setAttribute("scale", "0.005 0.005 0.005");
        plakkertjesNeergelegd.setAttribute("rotation", "0 90 0");
        plakkertjesNeergelegd.setAttribute("position", "6.3 1.1 -1.3");

        scene.appendChild(plakkertjesNeergelegd);
        addListeners();
        hold = null;
        document.getElementById('js--hold').remove();
      }
    });
  }
}
